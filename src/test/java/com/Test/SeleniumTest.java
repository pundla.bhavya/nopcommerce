package com.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;


import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;

public class SeleniumTest {
	
	static WebDriver driver;
	@BeforeClass
	private void launchApplication() {
		
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://admin-demo.nopcommerce.com/login");
		
	}
	@Test
	public void Login() {
		driver.findElement(By.className("email")).clear();
		driver.findElement(By.className("email")).sendKeys("admin@yourstore.com");
		driver.findElement(By.className("password")).sendKeys("admin");
		driver.findElement(By.id("RememberMe")).click();
		driver.findElement(By.xpath("//button[text()='Log in']")).click();
		driver.findElement(By.xpath("//button[text()='Log in']")).click();
		WebElement john = driver.findElement(By.linkText("John Smith"));
		String text=john.getText();
		Assert.assertEquals(text,"John Smith");
	}
	@Test(priority = 2)
	public void Catalog() throws IOException {
		driver.findElement(By.partialLinkText("Catalog")).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.findElement(By.xpath("//p[contains(text(),' Categories')]")).click();
		driver.findElement(By.partialLinkText("Add new")).click();
		File f=new File("/home/pundla/Music/TestFile/TestData1.xlsx");
		FileInputStream fis= new FileInputStream(f);
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		XSSFSheet sheet=workbook.getSheetAt(0);
		
		
		int rows=sheet.getPhysicalNumberOfRows();
		for(int i=1;i<rows;i++) {
			String name=sheet.getRow(i).getCell(0).getStringCellValue();
			String description=sheet.getRow(i).getCell(1).getStringCellValue();
			double numericCellValue=sheet.getRow(i).getCell(2).getNumericCellValue();
			driver.findElement(By.id("Name")).sendKeys(name);
			driver.findElement(By.id("description_ifr")).sendKeys(description);
			WebElement parentcatalog=driver.findElement(By.id("ParentCatalogId"));
			Select sl=new Select(parentcatalog);
			sl.selectByVisibleText("Computers >> Build your own computer >> Computers>>Desktops");
			WebElement price = driver.findElement(By.xpath("//label[@for=\"PriceFrom\"]"));
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView()",price);
			driver.findElement(By.id("PriceFrom")).sendKeys(numericCellValue);
		}
	}
			
	
	
	@Test(priority = 3)
	public void Products() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.partialLinkText("Products")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		WebElement DropDown = driver.findElement(By.id("SearchCategoryId"));
		Select sl = new Select(DropDown);
		sl.selectByIndex(2);
		driver.findElement(By.id("search-products")).click();
		
	}
	@Test(priority = 4)
	public void Manufactures() throws Exception {
		driver.findElement(By.partialLinkText("Manufacturers")).click();
		Thread.sleep(3000);
		File f = new File("/home/pundla/Music/TestFile/TestData1.xlsx");
		FileInputStream fis = new FileInputStream(f);
		Workbook book = Workbook.getWorkbook(fis);
		Sheet sh = book.getSheet("Sheet2");
		int rowCount = sh.getRows();
		int columnsCount = sh.getColumns();
		for (int i = 1; i < rowCount; i++) {
			String name = sh.getCell(0, 1).getContents();
			String description = sh.getCell(1, 1).getContents();
			driver.findElement(By.id("Name")).sendKeys(name);
			driver.findElement(By.id("Description_ifr")).sendKeys(description);
			driver.findElement(By.xpath("//a[@href='/Admin/Manufacturer/Create']")).click();
			driver.findElement(By.xpath("//input[@id='Name']")).sendKeys(name);
			driver.findElement(By.id("Description_ifr")).sendKeys(description);
			driver.findElement(By.xpath("//button[1][@class='btn btn-primary']")).click();
			Thread.sleep(3000);
			driver.findElement(By.id("SearchManufacturerName")).sendKeys("HP");
			
			driver.findElement(By.id("search-manufacturers")).click();
		}
	}
	@Test(priority = 5)
	public void Logout() {
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
	}
	@AfterClass
	public void afterClass() {
		driver.close();
	}
}